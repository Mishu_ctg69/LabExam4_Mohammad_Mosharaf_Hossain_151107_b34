<script type = "text/javascript">

    function isPrime(){
        var list = "2";
        for ( var number = 3; number<=100; number += 2 )
        {
            var prime = true;
            for ( var i = 3; i <= Math.sqrt(number);  i += 2 ){
                if ( number % i == 0 )
                {
                    prime = false;
                    break;
                }
            }
            if (prime) { list += "<br>" + number;
            }
        }
        document.write( "Prime numbers are: <br>" + list );
    }

    isPrime();

</script>