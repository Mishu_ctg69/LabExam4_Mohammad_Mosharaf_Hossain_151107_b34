<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


    <!--script for 4 -->

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
    </style>


    <style type="text/css">
        .bs-example{
            margin: 150px 50px;
        }
    </style>



</head>

<body>


<!-- plugin 4 -->
<h2>Display page</h2>

<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="image/image.jpg" alt="flower" width="100%" height="400">
                <?php
                $image_name="image.jpg";
                $temporary_location=$_FILES['image']['tmp_name'];
                $mylocation='image/'.$image_name;
                move_uploaded_file($temporary_location,$mylocation);
                ?>
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description'];?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>
            <div class="item">
                <img src="image/image.jpg" alt="flower" width="100%" height="400">
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description'];?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>

            <div class="item">
                <img src="image/image.jpg" alt="flower" width="100%" height="400">
                <div class="carousel-caption">
                    <h3><?php echo $_POST['description'];?></h3>
                    <p><?php echo $_POST['date']; ?></p>
                </div>
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
</body>
</html>
